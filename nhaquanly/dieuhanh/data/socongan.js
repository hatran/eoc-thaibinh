﻿$(function () {
    // Radialize the colors
    var chart = new Highcharts.setOptions({
        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {
                    cx: 0.5,
                    cy: 0.3,
                    r: 0.7
                },
                stops: [
                  [0, color],
                  [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
            };
        })
    });

    // Build the chart
    var chart = new Highcharts.chart('container1', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([18, 11, 14, 3,4,4,46]);
            //                //chart.series[1].setData([10.57]);
            //                //chart.series[2].setData([7.23]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0, 0, 0, 0,0,0,0]);
            //                //chart.series[1].setData([0]);
            //                //chart.series[2].setData([0]);
            //                count = 0;
            //            }
            //        }, 3000);
            //    }
            //}
        },
        title: {
            text: 'Báo cáo tình hình tội phạm 05/2019',
            style: {
                color: '#000000',
                fontWeight: 'bold'
            }
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        //colors: ['lightgray', 'blue', 'orange', 'green'],
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        series: [{
            name: '',
            colorByPoint: true,
            data: [{
                name: 'Cố ý gây thương tích',
                y: 18,
                sliced: true,
                selected: true
            }, {
                name: 'Cướp giật tài sản',
                y: 11
            }, {
                name: 'Cướp tài sản',
                y: 14
            }, {
                name: 'Hiếp dâm, cưỡng dâm',
                y: 3
            }, {
                name: 'Giết người',
                y: 4
            }, {
                name: 'Mua bán vận chuyển ma túy trái phép',
                y: 4
            }, {
                name: 'Khác',
                y: 46
            }
            ]
        }]
    });
});