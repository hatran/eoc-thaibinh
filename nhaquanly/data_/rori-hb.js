﻿$(function () {
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'rori',
        type: 'column',
        options3d: {
            enabled: true,
            alpha: 0,
            beta: 30,
            depth: 50,
            viewDistance: 25
        },
    },
    title: {
        text: null,
        style: {
            color: '#000000',
            fontWeight: 'bold'
        }
    },
    subtitle: {
        text: null
    },
    xAxis:{
        categories: ['TP. Hòa Bình', 'Kim Bôi', 'Tân Lạc', 'Mai Châu']
    },
    yAxis: {
        max: 50,
        title: {
            text: null
        }
    },
    colors: ['#38859B', '#46A1B9', '#7CBBCF', '#B5D5E1', '#CDE2EB', '#D9EAF0', '#4F6096', '#6C6550', 'green', 'lightblue', 'lightgreen'],

    plotOptions: {
        pie: {
            dataLabels: {
                enabled: false,
            }
        },
        column: {
            depth: 40,
            colorByPoint:true
        }
    },
    series: [{
        showInLegend:false,
        data: [50, 30,20,10 ]
    },
    ],
    credits: {
        enabled: false
    },
    exporting: { enabled: false }
});

function showValues() {
    $('#alpha-value').html(chart.options.chart.options3d.alpha);
    $('#beta-value').html(chart.options.chart.options3d.beta);
    $('#depth-value').html(chart.options.chart.options3d.depth);
}

// Activate the sliders
$('#sliders input').on('input change', function () {
    chart.options.chart.options3d[this.id] = parseFloat(this.value);
    showValues();
    chart.redraw(false);
});

showValues();
});