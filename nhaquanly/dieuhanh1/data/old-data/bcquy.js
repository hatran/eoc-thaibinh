﻿$(function () {

    // Create the chart
    var chart = new Highcharts.chart('container', {
        chart: {
            type: 'column',
            events: {
                load: function () {
                    var count = 0;
                    setInterval(function () {
                        if (count == 0) {
                            chart.series[0].setData([50, 25, 5, 20]);
                            //chart.series[1].setData([10.57]);
                            //chart.series[2].setData([7.23]);
                            count = 1;
                        }
                        else {
                            chart.series[0].setData([0, 0, 0, 0]);
                            //chart.series[1].setData([0]);
                            //chart.series[2].setData([0]);
                            count = 0;
                        }
                    }, 2000);
                }
            }
        },
        title: {
            text: 'Biểu đồ tổng kết Quý I về tình hình tội phạm trên địa bàn',
            style: {
                color: '#000000',
                //fontWeight: 'bold'
            }
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Cướp tài sản', 'Cố ý gây thương tích', 'Giết người', 'Khác'],
            
            labels: {
                autoRotation: 0,
                
            }
        },
        yAxis: {
            max:50,
            title: {
                text: '%'
            }
        },
        colors: ['green', 'red', 'blue', 'yellow'],

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        tooltip: {
            headerFormat: '<span style="font-size:11px"></span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
        },

        "series": [
          {
              //"name": "Browsers",
              "colorByPoint": true,
              "data": [
                {
                    "name": "Cướp tài sản",
                    "y": 50,
                },
                {
                    "name": "Cố ý gây thương tích",
                    "y": 25,
                },
                {
                    "name": "Giết người",
                    "y": 5,
                },
                {
                    "name": "Khác",
                    "y": 20,
                }
              ]
          }
        ],
        
    });
    
});